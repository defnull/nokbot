PHONY: run

run: venv
	venv/bin/python nokbot.py

venv: venv/bin/activate
venv/bin/activate: requirements.txt
	test -d venv || python3 -mvirtualenv venv
	venv/bin/pip install -Ur requirements.txt
	touch venv/bin/activate
