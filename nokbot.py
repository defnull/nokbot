#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import logging
import getpass
from optparse import OptionParser
import weakref
import sleekxmpp
from sleekxmpp.jid import JID
import os.path
import time
import datetime
import configparser
import random
import shlex
import inspect
import glob
import os


log = logging.getLogger(__name__)

def main():

    # Setup logging.
    logging.basicConfig(filename='nokbot.log',level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-8s %(message)s')

    # Setup the MUCBot and register plugins. Note that while plugins may
    # have interdependencies, the order in which you register them does
    # not matter.
    jid = os.environ.get('MUC_JID') or input('Jid: ').strip()
    pwd = os.environ.get('MUC_PASSWORD') or getpass.getpass('Password: ')

    # Let's stop at a specific time for automatic reloading.
    # This should get us back into the MUC after the Zwangstrennung.
    bis_h = int(os.environ.get('BIS_H') or "6") # hour
    bis_m = int(os.environ.get('BIS_M') or "0") # minute
    bis = datetime.time(bis_h, bis_m,0)  # stop at time H,M,S
    rfa = datetime.timedelta(minutes=20) # run for at least
    now = datetime.datetime.now()
    out = datetime.datetime.combine(now.date(), bis)
    if out < now + rfa:
        out = out + datetime.timedelta(days=1)
    # Time in seconds comparable with the result of time.time()
    run_until = time.mktime(out.timetuple())

    with MUCBot(jid, pwd, run_until) as bot:

        @bot.bind('*')
        def logall(names, *args):
            log.debug('Event %r: %r', names, args)

        bot.join('noklab@muc.cccgoe.de/heizung')

        with open('./brain.py', "rb") as fp:
            brain = compile(fp.read(), './brain.py', 'exec')
            exec(brain, {'bot':bot, 'log':logging.getLogger('brain')})

        bot.wait()




class Message(object):
    def __init__(self, bot, stanza, user, room=None):
        self.bot = weakref.proxy(bot)
        self.stanza = stanza
        self.src = weakref.proxy(user)
        self.room = weakref.proxy(room) if room else None

    @property
    def body(self):
        return self.stanza['body']
    
    def answer(self, text):
        if self.room:
            text = '%s: %s' % (self.src.nick, text)
            self.room.send(text)
        else:
            self.src.send(text)
        

class Room(object):
    def __init__(self, bot, jid):
        self.bot = weakref.proxy(bot)
        self.jid = jid
        self.users = dict()

    def bind(self, event, func=None):
        return self.bot.bind('%s:%s' % (self.name, event), func)

    def send(self, text):
        self.bot.send_message(mto=self.name, mbody=text, mtype='groupchat')

    @property
    def joined(self):
        return self.nick in self.users

    @property
    def nick(self):
        return self.jid.resource

    @property
    def name(self):
        return self.jid.bare

    def __repr__(self):
        return '<room %s>%s</room>' % self.jid, ' '.join(self.users.values())



class User(object):
    def __init__(self, bot, jid, room=None):
        self.bot = weakref.proxy(bot)
        self.jid = jid
        self.room = weakref.proxy(room) if room else None
        self.presence = None

    def send(self, text):
        self.bot.send_message(mto=self.jid, mbody=text, mtype='chat')

    @property
    def status(self):
        return self.presence['type'] if self.presence else 'unknown'

    @property
    def nick(self):
        return self.jid.resource
        
    def __repr__(self):
        return '<user %s (%s)>' % (self.jid, self.status)



class Command(object):
    def __init__(self, name, callback, aliases=None):
        self.callback   = callback
        self.name       = name
        self.aliases    = aliases or []
        self.signature  = inspect.signature(callback)

    def handle(self, msg, args):

        required = [p.name for p in self.signature.parameters.values()
                    if p.kind == p.POSITIONAL_OR_KEYWORD
                    and p.default is p.empty]
        optional = [p.name for p in self.signature.parameters.values()
                    if p.kind == p.POSITIONAL_OR_KEYWORD
                    and p.name not in required]
        additional = any(p.name for p in self.signature.parameters.values()
                    if p.kind == p.VAR_POSITIONAL)

        required.pop(0)
        log.debug([required, optional, additional])

        if len(args) < len(required):
            msg.answer("Not enough arguments. Call signature is: %s %s" % 
                (self.name, self.get_callspec()))
            return
        elif not additional and len(args) > len(required) + len(optional):
            msg.answer("To many arguments. Call signature is: %s %s" % 
                (self.name, self.get_callspec()))
            return

        try:
            self.callback(msg, *args)
        except Exception as e:
            msg.answer("Oops! You encountered a bug. I logged an exception.")
            log.exception('Error in command handler for %r', self)

    def get_callspec(self):
        params = [str(p) for p in self.signature.parameters.values()]
        result = " ".join(params[1:])
        return result

    def show_help(self, msg):
        msg.answer("Command usage: %s %s" % (self.name, self.get_callspec()))
        if self.aliases:
            msg.answer("Aliases: %s" % ', '.join(self.aliases))
        msg.answer("Help text: %s" % (self.callback.__doc__ or 'Not available'))

    def repr(self):
        return repr(self.callback)



class MUCBot(object):

    """
    A simple SleekXMPP bot that will greets those
    who enter the room, and acknowledge any messages
    that mentions the bot's nickname.
    """

    def __init__(self, jid, password, timeout=None):
        self.xmpp = sleekxmpp.ClientXMPP(jid, password)
        self.handler = {}
        self.commands = {}
        self.expose('help h', self.__help_command)
        self.rooms = dict()
        self.users = dict()
        self.timeout = timeout

        self.xmpp.register_plugin('xep_0030') # Service Discovery
        self.xmpp.register_plugin('xep_0045') # Multi-User Chat
        self.xmpp.register_plugin('xep_0199') # XMPP Ping
        self.xmpp.add_event_handler("session_start", self._on_session_start)
        self.xmpp.add_event_handler("message", self._on_message)
        self.xmpp.add_event_handler("presence", self._on_presence)
        self.xmpp.add_event_handler("disconnected", self._on_disconnect)

    def __enter__(self):
        if self.xmpp.connect():
            self.xmpp.process()
        else:
            raise RuntimeError("waaaah!")
        return self

    def wait(self):
        while self.xmpp.state.current_state() == 'connected':
            time.sleep(1)
            if self.timeout and self.timeout <= time.time():
                break

    def __exit__(self, exc_type, exc_obj, traceback):
        self.xmpp.disconnect(wait=True)

    def _on_session_start(self, event):
        self.xmpp.send_presence()
        self.xmpp.get_roster()
        if self.rooms: # We have reconnected a previous session
            for room in self.rooms.values():
                self.xmpp.plugin['xep_0045'].leaveMUC(jid.bare, jid.resource, 'reconnect')
                self.join(room.jid)
        self.handle('init')

    def _on_disconnect(self, event):
        self.handle('exit')

    def join(self, room):
        jid = JID(room)
        self.rooms[jid.bare] = Room(self, jid)
        self.xmpp.plugin['xep_0045'].joinMUC(jid.bare, jid.resource, wait=True)

    def send_message(self, **kwargs):
        self.xmpp.send_message(**kwargs)

    def _on_message(self, stanza):
        if stanza['type'] == 'groupchat':
            room = self.rooms.get(stanza['from'].bare)

            if not room:
                log.warning('Group message from room we are not in: %s', jid)
                return

            if not stanza['from'].resource: # Room broadcast
                log.info('Room message: %r', stanza)
                return

            user = room.users.get(stanza['from'].resource)

            if not user:
                log.warning('Group message from non-participant: %r', stanza)
                return

            if user.nick == room.nick: # echo
                return

            msg = Message(self, stanza, user, room)
            self.handle(['msg', '%s:msg' % room.name], msg)
            self.handle_command(msg)
        elif stanza['type'] == 'chat':
            user = self.users.get(stanza['from'].resource)
            if not user:
                user = self.users[stanza['from'].resource] = User(self, stanza['from'], None)
            msg = Message(self, stanza, user, None)
            self.handle(['msg'], msg)
            self.handle_command(msg)
        else:
            log.debug('Message: %r', stanza)

    def _on_presence(self, stanza):
        status = stanza['type']
        joined = status == 'available'

        if stanza['muc']:
            room = self.rooms.get(stanza['from'].bare)

            if not room:
                log.warning('Presence from room we are not in: %s', stanza)
                return
                
            user = room.users.get(stanza['from'].resource)

            if not user:
                user = User(self, stanza['from'], room)
                room.users[user.nick] = user

            old_status = user.status
            user.presence = stanza
            self.handle('status', user, old_status)
        else:
            log.debug('Someone sent us a private presence?? %r', stanza)

    def handle(self, event, *args):
        events = [event] if isinstance(event, str) else list(event)
        for event in events:
            for handler in self.handler.get(event, ()):
                try:
                    handler(*args)
                except Exception as e:
                    log.exception('Error in handler for %s', event)
                    continue

        for handler in self.handler.get('*', ()):
            try:
                handler(events, *args)
            except Exception as e:
                log.exception('Error in handler for %s', event)
                continue

    def bind(self, event, cb=None):
        def decorator(func):
            self.handler.setdefault(event, []).append(func)
            return func
        return decorator if cb is None else decorator(cb)

    def expose(self, names=None, callback=None):
        def decorator(func):
            aliases = (names or func.__name__).lower().split()
            cmd = Command(aliases[0], func, aliases[1:])
            for alias in aliases:
                assert alias.islower()
                assert alias.isalnum()
                self.commands[alias] = cmd
            return func
        return decorator(callback) if callback else decorator

    def handle_command(self, msg):
        args = shlex.split(msg.body.strip())
        cmd = args.pop(0).lower()
        if msg.room and cmd.startswith(msg.room.nick.lower()) and args:
            cmd = args.pop(0).lower()
        elif cmd[0] in '!/\\':
            cmd = cmd[1:]
        elif msg.room:
            return

        if not cmd.isalnum():
            return

        if cmd in self.commands:
            self.commands[cmd].handle(msg, args)
            return True

    def __help_command(self, msg, topic='commands'):
        ''' documentation '''
        if topic == 'commands':
            cmdlist = ', '.join(sorted(set(cmd.name for cmd in self.commands.values())))
            msg.answer('I know the following commands: %s' % cmdlist)
            msg.answer("Try 'help somecommand' to see somecommand's help text")
            msg.answer("If you see 'param=value' in the usage, this simply means")
            msg.answer("that this particular parameter has a default value.")
        elif topic in self.commands:
            self.commands[topic].show_help(msg)
        else:
            msg.answer("That is not a command I know.")


if __name__ == '__main__':
    main()

