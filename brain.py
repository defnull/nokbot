# -*- coding: utf-8 -*-

log.debug('Brain loaded...')
log.debug('Bot: %r', bot)

FILE_NOW = "/var/therm/now"
SCHEDULE_PATH = "/root/pyro/heizung"

# We used to import "subprocess" for calling the schedule.py script
# But this was kinda slow and the bot's response time was horrible.

import io  # for StringIO

import sys
sys.path.insert(0,SCHEDULE_PATH)
import schedule  # directly import the script!

def invoke_schedule_command(fun, *args):
    """invoke schedule command and return a (str, bool)
       as output text + success flag."""
    try:
        backup = sys.stdout
        textio = io.StringIO()
        sys.stdout = textio
        fun(*args)
    except Exception as x:
        errtext = str(x)
        if len(errtext) == 0:
            return ("No success.", False)
        else:
            return ("No success: " + errtext, False)
    finally:
        sys.stdout = backup
    return (textio.getvalue().rstrip(), True)

@bot.expose()
def reload(msg):
    """Reloads the bot. This involves syncing with the BitBucket repo for updates."""
    msg.bot.xmpp.disconnect(wait=True)

@bot.expose()
def ping(msg, topic=None):
    """Really? You want help for the ping command?"""
    msg.answer('Pong %r' % topic)

@bot.expose()
def status(msg, option=None):
    """status simply tells you the current and desired room temperatures"""
    try:
        with open(FILE_NOW, 'r') as fp:
            ctemp = float(fp.read().strip())
    except:
        msg.answer("I could not look up the current room temperature. Weird.")
        log.exception('status handler borked in reading therm/now')
        return

    params = ["max3"]
    if option != None:
        params.append(option)
    out, flag = invoke_schedule_command(schedule.command_show, params)
    msg.answer((u"The room temperature is %.1f°C. " % ctemp) + out)

@bot.expose()
def kuschelig(msg, temp="on", time="now", date="today"):
    """The kuschelig command allows you to
    control the temperature of the Noklab. Events can be queued for now or
    later. This command takes up to three parameters (all of them optional):
    temp: Temperature in Celsius, "on" or "off"
    time: The time you plan to arrive at the Noklab (e.g. "19:00")
    date: On which day you plan to arrive (e.g. "tomorrow", "2015-02-27")"""

    if temp == "help":
        msg.answer(kuschelig.__doc__)
        return

    if time == "now":
        time = None
        date = None

    if time != None and date != None:
        time = time + ", " + date

    params = list(filter(lambda x: x!=None, [temp, time]))
    out, flag = invoke_schedule_command(schedule.command_add, params)
    msg.answer(out)

@bot.expose()
def kuschdel(msg, indices):
    """The kuschdel command allows you to delete scheduled events.
    Here are some examples of what this might look like:
    !kuschdel 1     (removes event 1 from schedule)
    !kuschdel 2     (removes event 2 from schedule)
    !kuschdel 1,2   (removes event 1 and 2)"""

    if indices == "help":
        msg.answer(kuschdel.__doc__)
        return

    params = [indices]
    out, flag = invoke_schedule_command(schedule.command_del, params)
    msg.answer(out)
